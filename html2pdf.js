const puppeteer = require("puppeteer");
const path = require("path");
const { exit } = require("process");


async function generatePDF(url, output) {
  var options = {
    path: `${output}/Features.pdf`,
    format: "A4",
    margin: {
      top: 64,
      bottom: 64,
      right: 48,
      left: 48,
    },
  };

  try {
    // Launch a new browser session.
    const browser = await puppeteer.launch({headless: true});
    // Open a new Page.
    const page = await browser.newPage();
    // Go to our invoice page that we serve on `localhost: 8000`.
    if (process.platform === 'win32') {
      await page.goto(`file://${path.resolve(url.replace(/[\\]/g, '/'))}`, {waitUntil: 'domcontentloaded'});
    } else {
      await page.goto(`file://${path.resolve(url)}`, {waitUntil: 'domcontentloaded'});
    }
    // Save the page as PDF.
    await page.pdf(options);
    await browser.close();
    console.log(`PDF successfully created in -> ${path.resolve(options.path)}`);
  } catch (e) {
    console.log(`ERRO AO GERAR PDF`);
    console.log(`HTML path ${url}`);
    console.log(`HTML url replace ${url.replace(/[\\]/g, "/")}`);
    console.error(e);
  }

  console.log('Finish process!');
}

module.exports = {
  generatePDF
}
