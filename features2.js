#!/usr/bin/env node

// MODULES
const commander = require('commander');
const fs = require('fs');
const glob = require('glob');
const handlebars = require('handlebars');
const linereader = require('line-reader');
const path = require('path');
const async = require('async');
const moment = require('moment');
const i18n = require('i18next');
const config = require('./config');
const pdf = require('./html2pdf');

// options
commander
  .version(require('./package.json').version)
  .option('-a, --author [string]', 'The author name used in header (default: John Doe)')
  .option('-b, --break-before-word [string]', 'create a line break before every occurrance of this word in the background (default: null)')
  .option('-d, --product-description [string]', 'The description of your product used in header (default: My Product Description)')
  .option('-g, --generate-pdf [false|true]', ' generate PDF file(default: true)')
  .option('-l, --lang [en|pt|string]', 'language used in feature files (default: pt)')
  .option('-p, --product-name [string]', 'The name of your product used in headline and header (default: My Product Name)')
  .option('-t, --templates-dir [path]', 'read the files doc_template.html, feature_template.html and style.css from path (default: default/templates)')
  .option('-x, --debug', 'output extra debugging')


// commands
commander
  .requiredOption('-i, --input-dir [path]', 'read feature files from path (default: examples/features)')
  .requiredOption('-o, --output-file [path]', 'send output to file path (default: target/feature.html)')
  .command('create')
  .option('-a, --author [string]', 'The author name used in header (default: John Doe)')
  .option('-b, --break-before-word [string]', 'create a line break before every occurrance of this word in the background (default: null)')
  .option('-d, --product-description [string]', 'The description of your product used in header (default: My Product Description)')
  .option('-g, --generate-pdf [false|true]', ' generate PDF file(default: true)')
  .option('-l, --lang [en|pt|string]', 'language used in feature files (default: pt)')
  .option('-p, --product-name [string]', 'The name of your product used in headline and header (default: My Product Name)')
  .option('-t, --templates-dir [path]', 'read the files doc_template.html, feature_template.html and style.css from path (default: default/templates)')
  .option('-x, --debug', 'output extra debugging')
  .description('Create HTML or PDF from feature files')
  .action(createCommand);

// Check if called without command
if (process.argv.length < 3) {
  commander.help();
}

function resolvePath(folderPath) {
  return path.resolve(folderPath.replace(/^["|'](.*)["|']$/, '$1'));
}
// parse commands
commander.parse(process.argv);
//debug commander
if (commander.debug) console.log(commander.opts());

function setup(done) {
  config.INPUTDIR = resolvePath(commander.inputDir) || config.INPUTDIR;
  config.TEMPLATESDIR = commander.templatesDir || config.TEMPLATESDIR;
  config.OUTPUTFILE = resolvePath(commander.outputFile) || config.OUTPUTFILE;
  config.LANGUAGE = commander.lang || config.LANGUAGE;
  config.AUTHOR = commander.author || config.AUTHOR;
  config.PRODUCTNAME = commander.productName || config.PRODUCTNAME;
  config.PRODUCTDESCRIPTION = commander.productDescription || config.PRODUCTDESCRIPTION;
  config.BREAKBEFOREWORD = commander.breakBeforeWord || config.BREAKBEFOREWORD;
  config.GENERATEPDF = commander.generatePdf || config.GENERATEPDF;
  config.DOCTEMPLATE = config.TEMPLATESDIR + '/doc_template.html';
  config.FEATURETEMPLATE = config.TEMPLATESDIR + '/feature_template.html';

  i18n.init({
    lng: config.LANGUAGE,
    debug: false,
    fallbackLng: ['pt'],
    load: config.LANGUAGE,
    returnNull: true,
    returnEmptyString: true,
    returnObjects: true,
    joinArrays: true,
    resources: require('./locales/translation.json'),
  }, (err, t) => {
    if (err) return console.error(err)
    done();
  });
}

function createCommand() {
  setup(create);
}

async function create() {
  var pathHTMLTemplate = path.resolve(path.join(__dirname, config.DOCTEMPLATE));
  var pathHTMLFeatureTemplate = path.resolve(path.join(__dirname, config.FEATURETEMPLATE));
  var pathStyle = path.resolve(path.join(__dirname, config.TEMPLATESDIR + '/style.css'));

  var docHandlebarTemplate = handlebars.compile(fs.readFileSync(pathHTMLTemplate, config.FILE_ENCODING));
  var featureHandlebarTemplate = handlebars.compile(fs.readFileSync(pathHTMLFeatureTemplate, config.FILE_ENCODING));
  var cssStyles = fs.readFileSync(pathStyle, config.FILE_ENCODING);

  parseFeatures(function (features) {
    var featuresHtml = '';
    if (features) {
      for (var i = 0; i < features.length; i++) {
        featuresHtml += featureHandlebarTemplate(features[i]);
      }
    }
    var docData = new Object();
    docData.cssStyles = cssStyles;
    docData.creationdate = moment().format('LL');
    docData.author = config.AUTHOR;
    docData.productname = config.PRODUCTNAME;
    docData.productdescription = config.PRODUCTDESCRIPTION;
    docData.featuresHtml = featuresHtml;
    var docHtml = docHandlebarTemplate(docData);
    // write to default output dir. Create first if necessary
    console.log(config.OUTPUTFILE);
    fs.mkdir(config.OUTPUTFILE, { recursive: true }, async function (e) {
      if (!e || (e && e.code === 'EEXIST')) {
        var outputFilepath = `${config.OUTPUTFILE}/Features.html`;
        writeOutput(docHtml, outputFilepath).then((value) => {
          console.log('HTML successfully created in -> ' + path.resolve(value))
          if (config.GENERATEPDF == true || config.GENERATEPDF == "true") {
            pdf.generatePDF(path.resolve(outputFilepath), config.OUTPUTFILE);
          } else {
            console.log('Finish process!');
          }
        });
      } else {
        console.log(e);
      }
    });
  });
}

function writeOutput(html, outputfile) {
  return new Promise(function (resolve, reject) {
    fs.writeFile(outputfile, html, config.FILE_ENCODING, function (err) {
      if (err) reject(err);
      else resolve(outputfile);
    });
  });
}

function parseFeatures(callback) {
  var featureFiles = [];
  if (fs.lstatSync(config.INPUTDIR).isDirectory() == true) {
    featureFiles = glob.sync(`${config.INPUTDIR}/**/*.feature`, { absolute: path.isAbsolute(config.INPUTDIR) }).sort();
  } else {
    if (fs.lstatSync(config.INPUTDIR).isFile() == true && config.INPUTDIR.endsWith('.feature')) {
      featureFiles = [config.INPUTDIR];
    } else {
      console.error("Input path invalid, expect a directory or file .feature. But found " + config.INPUTDIR);
      process.exit(1);
    }
  }
  async.mapSeries(featureFiles, parseFeatureFile, function (err, results) {
    callback(results);
  });
}

function parseFeatureFile(featureFilename, callback) {

  var feature = new Object();
  feature.background = '';
  feature.scenarios = [];
  var scenario = new Object();
  scenario.content = '';

  var foundMultirowScenario = false;
  var featureLineWasFound = false;
  var scenariosStarted = false;

  linereader.eachLine(featureFilename, function (line) {

    if (lineIndicatesThatANewScenarioBegins(line) && foundMultirowScenario) {
      // new scenario found. start parsing new scenario
      feature.scenarios.push(scenario);
      scenario = new Object();
      scenario.content = '';
      foundMultirowScenario = false;
      scenariosStarted = true;
    }

    if (lineIndicatesThatANewScenarioBegins(line) || foundMultirowScenario) {
      // We are parsing a scenario. It may be a new scenario or a row within a scenario
      foundMultirowScenario = true;
      scenariosStarted = true;

      // Handle sidenote
      if (i18nStringContains(line, 'sidenote')) {
        scenario.sidenote = line.replace(i18n.t('sidenote'), '');
      } else {
        // Handle scenario content
        if (scenario.content) {
          scenario.content = scenario.content + '\n' + line;
        } else {
          scenario.content = line;
        }
      }

    }

    if (!i18nStringContains(line, 'feature') && !scenariosStarted && featureLineWasFound) {
      // Everything between feature and first scenario goes into feature.background, except background keyword
      var fixedline = config.BREAKBEFOREWORD ? line.replace(config.BREAKBEFOREWORD, '</p><p class="p-after-p">' + config.BREAKBEFOREWORD) : line;
      fixedline = fixedline + '<br/>';
      feature.background = feature.background + ' ' + fixedline.replace(i18n.t('background'), '');
    }

    if (i18nStringContains(line, 'feature')) {
      feature.name = line.replace(i18n.t('feature'), '');
      featureLineWasFound = true;
    }

  }).then(function () {
    // Add last scenario, if exists
    if (scenario && scenario.content) {
      feature.scenarios.push(scenario);
    }
    callback(null, feature);
  });

}

function lineIndicatesThatANewScenarioBegins(line) {
  return i18nStringContains(line, 'scenario') || i18nStringContains(line, 'scenario_outline') || i18nStringContains(line, 'sidenote') || i18nStringContains(line, 'background');
}

function i18nStringContains(orgstring, i18nkey) {
  var tr = i18n.t(i18nkey);
  if (Array.isArray(tr)) {
    return tr.map(v => orgstring.indexOf(v) !== -1).indexOf(true) !== -1
  } else {
    return orgstring.indexOf(i18n.t(i18nkey)) !== -1;
  }
}

module.exports = {
  output: [
    commander
  ]
}